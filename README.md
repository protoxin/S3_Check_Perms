# Check_Perms
This python script is designed to parse your Amazon Buckets to see if they are world readable.

Note: If you do something stupid with this, you did so knowingly and it is not my problem. If you don't know what this does, then don't use it. 

Usage: `./check_perms.py logfile.txt output.csv`

## Installation

Use pip, plz. 
`pip install -r requirements.txt`

## Additional Notes
Keep in mind that you will need an AWS API key so that you can actually access this data. 

There are probably other scripts our there that work better, however, I needed something custom and was bored. 
