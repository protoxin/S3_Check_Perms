#!/usr/bin/env python

"""
Description: Python script to read the log file of the bucket-stream tool and parse permissions.
Author: ProToxin
Version: 0.0.1b

Comments:

Currently, this is not threaded. There were some REALLY weird results with
returned Amazon data when we tried threading. Some parts of this list may
be more useful for your projects. The get_perms func. is probably the most
important part of this entire script/project.

New:

Added date part so that one can track when you actually parsed buckets. 

"""
from boto.s3.connection import S3Connection
from time import sleep
import os
import sys
import pyprind
from datetime import date


all_users = 'http://acs.amazonaws.com/groups/global/AllUsers'
# AWS API KEYS
conn = S3Connection('', '')
rs = conn.get_all_buckets()
# Our two sets that will allow for determining diffs and working with buckets.
lines_seen = set()
source_data = set()
today = date.today()
added = date.isoformat(today)


def get_perms(acl_data):
    """Function to parse ACLs for S3 buckets"""
    # Assume false
    publicbucket = False
    for grant in acl_data.acl.grants:
        #print grant
        # Check if the all_users flag is present.
        if grant.permission == 'READ':
            if grant.uri == all_users:
                # Now we know it is a public bucket :)
                publicbucket = True
        else:
            # Loop through the rest of the permission objects.
            continue
    if not publicbucket:
        param = bucket_name+".s3.amazonaws.com,N,%s" % added
        #print param
        a = open(sys.argv[2],'a')
        a.write(param+"\n")
        a.close()
    if publicbucket:
        readable = "Y"
        #print bucket_name+".s3.amazonaws.com,%s" % readable
        param = bucket_name+".s3.amazonaws.com,%s,%s" % (readable,added)
        a = open(sys.argv[2],'a')
        a.write(param+"\n")
        a.close()

def parse_list():
    """Function for parsing the output file and append to a set"""
    global lines_seen
    with open(sys.argv[2],'r') as destfile:
        for line in destfile:
            line = line.rstrip()
            line = line.split(',')
            lines_seen.add(line[0])

def parse_source():
    """Function for parsing the source file of buect names and appending to a set"""
    global source_data
    with open(sys.argv[1],'r') as sourcefile:
        for line in sourcefile:
            line = line.rstrip()
            source_data.add(line)

print "[+]Parsing lists..."
parse_list()
parse_source()
print "[+]Parsing complete. "
total = len(source_data-lines_seen)


print "[+]Checking if there are differences..."

#Since a set cannot have duplicate value, we subtrace the log file from the output. This shows us difference of unique values.
# If diff is not 0, then we know there are differences and diff is now a set object with only unique values.
diff = source_data-lines_seen

#use less than or equal if for some reason a new list is introduced with fewer things.
if len(diff) <= 0:
    print "[+]No new buckets..."
    print "[+]Exiting..."
    exit()
else:
    # Again, since we have a set, we can get the len of the array of unique values.
    print "[+]%s buckets to add" % len(diff)
# Set n to the len so we have a reference nubmer in our progress bar.
n = len(diff)
# Setup the bar. We'll track time so we have additional metrics.
bar = pyprind.ProgBar(n, track_time=True, title='[+]Enumerating ACLs')
threashold = 500
counter = 0
for buckete in diff:
    if counter == threashold:
        sleep(10)
        counter = 0
    else:
        pass
    # Rstrip just in case there is some weird extra character that was added
    buckete = buckete.rstrip()
    # Replace anything prior to the .s3 stuff, as then we can easily pass it on
    bucket_name = buckete.replace(".s3.amazonaws.com","")
    try:
        # Initate a connection and set validate to fales. This makes it so we don't waste an API hit.
        # besides, we already know it's a valid bucket.
        bucket = conn.get_bucket(bucket_name, validate=False)
        acl_data = bucket.get_acl()
        # Parse permissions :)
        get_perms(acl_data)
        #progress(i, total, status='Parsing ACL data...')
        bar.update(force_flush=True)
    # We will get a forbidden/denied message from Amazon, which causes an exception
    # if the bucket is fully restricted or denies us from pulling permissions.
    # Knowing this, we can assume it is not readable and can just say it isn't.
        counter += 1
    except:
        readable = "N"
        param = bucket_name+".s3.amazonaws.com,%s,%s" % (readable,added)
        a = open(sys.argv[2],'a')
        a.write(param+"\n")
        a.close()
        # Sleep for a second just to slow down our requests to Amazon
        #sleep(0.5)
        bar.update(force_flush=True)
        counter += 1
        continue

print "[+]Completed process. Go forth and plunder."
